use chrono::{DateTime, Datelike, Duration as ChronoDuration, Local, TimeZone, Timelike, Utc};
use std::time::Duration as StdDuration;
use tokio::time::Instant as TokioInstant;

const HOURS_PER_DAY: u32 = 24;
/// The update frequency (Splatoon 2: every 2 hours)
const GEAR_ROTATION_TIME: u32 = 2;
/// An extra delay to let the API update before fetching
const DELAY_BEFORE_FETCH_SECONDS: u32 = 30;

/// Compute the date & time of the next rotation, relatively to `reference_date_time`
fn next_update(reference_date_time: &DateTime<Utc>) -> anyhow::Result<DateTime<Utc>> {
    let next_update_hour = ((reference_date_time.hour() / GEAR_ROTATION_TIME + 1)
        * GEAR_ROTATION_TIME)
        % HOURS_PER_DAY;

    let next_dt = Utc.with_ymd_and_hms(
        reference_date_time.year(),
        reference_date_time.month(),
        reference_date_time.day(),
        next_update_hour,
        0,
        0,
    );

    if let Some(next_dt) = next_dt.single() {
        if next_dt.hour() < reference_date_time.hour() {
            // If the new hour is earlier than reference, next update occurs on the following day
            Ok(next_dt + ChronoDuration::days(1))
        } else {
            Ok(next_dt)
        }
    } else {
        Err(anyhow::anyhow!(
            "Got ambiguous or none date_time {:?}",
            next_dt
        ))
    }
}

/// Compute time remaining before the next rotation, including delay
fn time_until_next_update() -> anyhow::Result<StdDuration> {
    let now = Utc::now();
    let update = next_update(&now)?;
    let delay = ChronoDuration::seconds(DELAY_BEFORE_FETCH_SECONDS as i64);

    match (update - now + delay).to_std() {
        Ok(duration) => Ok(duration),
        Err(err) => Err(anyhow::anyhow!(err)),
    }
}

/// Wait asynchronously until next rotation
pub async fn wait_update() -> anyhow::Result<()> {
    let utc_update_time = next_update(&Utc::now())?;

    let local_update_time: DateTime<Local> = DateTime::from(utc_update_time);
    println!("Next Update: {}", local_update_time.format("%R"));

    tokio::time::sleep_until(TokioInstant::now() + time_until_next_update()?).await;
    Ok(())
}

#[cfg(test)]
mod tests {
    use std::fmt::Debug;

    use chrono::MappedLocalTime;

    use super::*;

    fn err_on_non_single<T: Debug>(dt: MappedLocalTime<T>) -> anyhow::Result<T> {
        match dt.single() {
            Some(x) => Ok(x),
            None => Err(anyhow::anyhow!("Date time is ambiguous or invalid")),
        }
    }

    fn create_date_time(
        year: i32,
        month: u32,
        day: u32,
        hour: u32,
        min: u32,
        sec: u32,
    ) -> anyhow::Result<DateTime<Utc>> {
        err_on_non_single(Utc.with_ymd_and_hms(year, month, day, hour, min, sec))
    }

    #[test]
    fn test_next_update() -> anyhow::Result<()> {
        let year = 2021;
        let month = 01;
        let day = 01;

        let dt = create_date_time(year, month, day, 0, 0, 0)?;
        assert_eq!(create_date_time(2021, 01, 01, 2, 0, 0)?, next_update(&dt)?);

        let dt = create_date_time(year, month, day, 4, 0, 0)?;
        assert_eq!(create_date_time(2021, 01, 01, 6, 0, 0)?, next_update(&dt)?);

        let dt = create_date_time(year, month, day, 5, 3, 0)?;
        assert_eq!(create_date_time(2021, 01, 01, 6, 0, 0)?, next_update(&dt)?);

        let dt = create_date_time(year, month, day, 1, 59, 59)?;
        assert_eq!(create_date_time(2021, 01, 01, 2, 0, 0)?, next_update(&dt)?);

        let dt = create_date_time(year, month, day, 23, 30, 0)?;
        assert_eq!(create_date_time(2021, 01, 02, 0, 0, 0)?, next_update(&dt)?);

        let dt = create_date_time(2021, 12, 31, 23, 40, 0)?;
        assert_eq!(create_date_time(2022, 01, 01, 0, 0, 0)?, next_update(&dt)?);

        Ok(())
    }
}
