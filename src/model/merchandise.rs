use crate::model::app_url::ImgAppUrl;
use bytes::Bytes;
use chrono::{DateTime, Local};
use reqwest::{Client, Result as ReqwestResult};
use serde::Deserialize;
use serde_with::{serde_as, TimestampSeconds};
use std::fmt;

#[serde_as]
#[derive(Deserialize, Debug)]
#[allow(dead_code)] // Keep all fields returned by the API, may be used later
pub struct Merchandise {
    skill: Skill,
    gear: Gear,
    id: String,
    #[serde_as(as = "TimestampSeconds<i64>")]
    end_time: DateTime<Local>,
    kind: Kind,
    price: u32,
    original_gear: OriginalGear,
}

impl fmt::Display for Merchandise {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.gear)
    }
}

impl Merchandise {
    fn available_until(&self) -> String {
        format!("Available until {}", self.end_time.format("%X"))
    }

    fn compare_skills(&self) -> SkillComparison {
        SkillComparison {
            actual: &self.skill,
            original: self.original_gear.skill(),
        }
    }

    fn skills(&self) -> String {
        format!("{}", self.compare_skills())
    }

    pub fn description(&self) -> String {
        self.skills() + "\n" + &self.available_until()
    }

    pub async fn get_image(&self, client: &Client) -> ReqwestResult<Bytes> {
        self.gear.image.get_image(client).await
    }
}

#[derive(Deserialize, Debug)]
#[allow(dead_code)] // Keep all fields returned by the API, may be used later
struct Gear {
    thumbnail: ImgAppUrl,
    kind: Kind,
    name: String,
    image: ImgAppUrl,
    brand: Brand,
    id: String,
    rarity: u8,
}

fn print_stars(nb_stars: usize) -> String {
    let mut s = String::with_capacity(nb_stars);

    for _ in 0..nb_stars {
        s.push('★');
    }
    s
}

impl fmt::Display for Gear {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} {}", self.name, print_stars(self.rarity as usize + 1))
    }
}

#[derive(Deserialize, Debug)]
#[allow(dead_code)] // Keep all fields returned by the API, may be used later
struct Brand {
    name: String,
    image: ImgAppUrl,
    id: String,
    frequent_skill: Skill,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all(deserialize = "snake_case"))]
enum Kind {
    Head,
    Clothes,
    Shoes,
}

#[derive(Deserialize, Debug)]
#[allow(dead_code)] // Keep all fields returned by the API, may be used later
struct Skill {
    id: String,
    image: ImgAppUrl,
    name: String,
}

impl fmt::Display for Skill {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.name)
    }
}

struct SkillComparison<'a, 'b> {
    actual: &'a Skill,
    original: &'b Skill,
}

impl fmt::Display for SkillComparison<'_, '_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} → {}", self.original, self.actual)
    }
}

#[derive(Deserialize, Debug)]
#[allow(dead_code)] // Keep all fields returned by the API, may be used later
struct OriginalGear {
    name: String,
    price: Option<u32>,
    rarity: u8,
    skill: Skill,
}

impl OriginalGear {
    pub fn skill(&self) -> &Skill {
        &self.skill
    }
}
