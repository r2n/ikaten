use bytes::Bytes;
use image::ImageReader;
use image::{DynamicImage, ImageFormat, ImageResult};
use std::io::Cursor;

const FORMAT: ImageFormat = ImageFormat::Png;

pub fn bytes_to_image(bytes: Bytes) -> ImageResult<DynamicImage> {
    let mut image_reader = ImageReader::new(Cursor::new(bytes));
    image_reader.set_format(FORMAT);
    image_reader.decode()
}
