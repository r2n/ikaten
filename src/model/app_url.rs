use bytes::Bytes;
use reqwest::{Client, Response, Result as ReqwestResult, Url};
use serde::de::{self, Unexpected, Visitor};
use serde::{Deserialize, Deserializer};
use std::fmt;

pub const BASE_URL: &str = "https://splatoon2.ink";
pub const BASE_IMAGE_PATH: &str = "/assets/splatnet";

#[derive(Debug)]
pub struct AppUrl {
    url: Url,
}

impl AppUrl {
    fn new(url: Url) -> AppUrl {
        AppUrl { url }
    }

    async fn get(&self, client: &Client) -> ReqwestResult<Response> {
        client.get(self.url.clone()).send().await
    }
}

impl<'de> Deserialize<'de> for AppUrl {
    fn deserialize<D>(deserializer: D) -> Result<Self, <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_str(AppUrlVisitor)
    }
}

struct AppUrlVisitor;

impl<'de> Visitor<'de> for AppUrlVisitor {
    type Value = AppUrl;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str("a URL path")
    }

    fn visit_str<'a, E>(self, value: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        let parsed_url = reqwest::Url::parse(&(BASE_URL.to_owned() + BASE_IMAGE_PATH + value));

        match parsed_url {
            Ok(u) => Ok(AppUrl::new(u)),
            Err(_err) => Err(E::invalid_value(
                Unexpected::Str(value),
                &"a valid path for a URL",
            )),
        }
    }
}

#[derive(Debug, Deserialize)]
#[serde(transparent)]
pub struct ImgAppUrl {
    app_url: AppUrl,
}

impl ImgAppUrl {
    pub async fn get_image(&self, client: &Client) -> ReqwestResult<Bytes> {
        self.app_url.get(client).await?.bytes().await
    }
}
