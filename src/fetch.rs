use crate::model::app_url::BASE_URL;
use crate::model::image_converter;
use crate::model::merchandise::Merchandise;
use crate::notifier;
use notify_rust::Image;
use reqwest::{Client, Error as ReqwestError};
use std::collections::HashMap;
use std::convert::TryFrom;
use std::error::Error;

const ENDPOINT: &str = "/data/merchandises.json";
const MAIN_PAYLOAD_KEY: &str = "merchandises";

static APP_USER_AGENT: &str = concat!(
    env!("CARGO_PKG_NAME"),
    "/",
    env!("CARGO_PKG_VERSION"),
    " ",
    env!("CARGO_PKG_HOMEPAGE")
);

pub fn build_client() -> Result<Client, ReqwestError> {
    Client::builder().user_agent(APP_USER_AGENT).build()
}

pub async fn fetch(client: &Client, app_icon: &Image) -> Result<(), Box<dyn Error>> {
    let resp = client
        .get(BASE_URL.to_owned() + ENDPOINT)
        .send()
        .await?
        .json::<HashMap<String, Vec<Merchandise>>>()
        .await?;

    let merchandises = match resp.get(MAIN_PAYLOAD_KEY) {
        Some(m) => Ok(m),
        None => Err(format!("Could not find key {}", MAIN_PAYLOAD_KEY)),
    }?;

    for merchandise in merchandises {
        let merch_icon = Image::try_from(image_converter::bytes_to_image(
            merchandise.get_image(client).await?,
        )?);
        let icon = merch_icon.unwrap_or_else(|_| app_icon.to_owned());
        notifier::notify(&icon, &merchandise.to_string(), &merchandise.description())?;
    }

    Ok(())
}
