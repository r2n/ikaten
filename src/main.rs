mod fetch;
mod model;
mod notifier;
mod wait;

use crate::fetch::build_client;
use crate::model::image_converter;
use bytes::Bytes;
use clap::Parser;
use image::{DynamicImage, ImageResult};
use notify_rust::Image;
use std::convert::TryFrom;
use std::error::Error;

#[derive(Parser)]
#[clap(version, about, long_about = None)]
/// Splatoon 2 Desktop Notifications
struct CliArgs {
    /// If set, the program returns after fetching instead of waiting and updating on every rotation
    #[clap(short, long)]
    exit: bool,
}

/// Load & convert the app icon to a usable format in notifications
fn load_icon() -> ImageResult<DynamicImage> {
    image_converter::bytes_to_image(Bytes::copy_from_slice(include_bytes!(
        "../assets/logo-512x512.png"
    )))
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let app_icon = Image::try_from(load_icon()?)?;
    let args = CliArgs::parse();
    let client = build_client()?;

    if args.exit {
        fetch::fetch(&client, &app_icon).await?;
        Ok(())
    } else {
        loop {
            fetch::fetch(&client, &app_icon).await?;
            wait::wait_update().await?;
        }
    }
}
