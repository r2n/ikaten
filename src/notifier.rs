use notify_rust::{Image, Notification, Timeout};

const NOTIFICATION_TIMEOUT_MS: u32 = 30 * 1000;

pub fn notify(icon: &Image, title: &str, message: &str) -> Result<(), Box<dyn std::error::Error>> {
    Notification::new()
        .appname("Ikaten")
        .summary(title)
        .body(message)
        .image_data(icon.to_owned())
        .timeout(Timeout::Milliseconds(NOTIFICATION_TIMEOUT_MS))
        .show()?;

    Ok(())
}
