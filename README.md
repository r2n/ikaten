# Ikaten – Splatoon 2 Desktop Notifications

<img src="assets/logo-text.svg" height="200" alt="Ikaten Logo" />

Ikaten is a work-in-progress notification tool for Splatoon 2.
It displays desktop notifications on every rotation using
[splatoon2.ink](https://splatoon2.ink/).

At the moment, notifications are limited to the current Splatnet gear.
Current maps and Salmon Run information should be added in the future.

## Supported platforms

Ikaten has only been tested on Linux (KDE) so far.
It should work similarly on other BSD/Linux desktop environments that follow the XDG
specification, such as Gnome, XFCE or LXDC.

However, Windows and macOS notifications may lack information due to
[notify-rust](https://github.com/hoodie/notify-rust) partially supporting them.

## Usage

By default, Ikaten fetches data from splatoon2.ink and sends notifications on every rotation (every 2 hours).
The process never exits.

```shell
./target/release/ikaten
```

To only retrieve information of the current rotation and exit, run:

```shell
./target/release/ikaten --exit
```

For more detail about the available flags:

```shell
./target/release/ikaten --help
```

## Building

In order to build the project, you must have Git and a recent Rust toolchain installed.

First, get the source code with:

```shell
git clone https://gitlab.com/r2n/ikaten
cd ikaten
```

To build Ikaten in release mode, use:

```shell
cargo build --release
```

You should now be able to run Ikaten:

```shell
./target/release/ikaten --version
```

See the "Usage" section for more detail.

## Licensing

Ikaten is licensed under the Apache 2.0 license. Third-party dependencies are listed in the `NOTICE` HTML file.

### Disclaimer: About splatoon2.ink & Non-Free Resources

This project fetches data from [splatoon2.ink](https://splatoon2.ink/).
While splatoon2.ink itself is released under the MIT License
([GitHub repository](https://github.com/misenhower/splatoon2.ink)),
most resources, including icons and names, are the property of Nintendo.

### Generating third-party licenses

Third-party licenses are available in the `NOTICE` file.
This file is generated automatically from the dependencies in `Cargo.toml`, using `cargo-about`.

To install `cargo-about`, run:

```shell
cargo install --locked cargo-about
```

Every time a dependency is added, removed or updated, run the following command to update the `NOTICE` file.

```shell
cargo about generate about.hbs > NOTICE
```
